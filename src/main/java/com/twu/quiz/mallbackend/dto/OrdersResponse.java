package com.twu.quiz.mallbackend.dto;

public class OrdersResponse {
    private Long orderId;
    private String name;
    private long price;
    private String unit;
    private int num;

    public OrdersResponse() {
    }

    public OrdersResponse(Long orderId, String name, long price, String unit, int num) {
        this.orderId = orderId;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.num = num;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public int getNum() {
        return num;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}
