package com.twu.quiz.mallbackend.repository;

import com.twu.quiz.mallbackend.domain.Goods;
import com.twu.quiz.mallbackend.domain.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long> {
    Optional<Orders> findByGoods(Goods goods);
}
