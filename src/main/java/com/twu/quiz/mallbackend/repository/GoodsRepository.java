package com.twu.quiz.mallbackend.repository;

import com.twu.quiz.mallbackend.domain.Goods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsRepository extends JpaRepository<Goods, Long> {
    List<Goods> findByName(String name);
}
