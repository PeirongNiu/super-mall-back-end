package com.twu.quiz.mallbackend.controller;

import com.twu.quiz.mallbackend.domain.Goods;
import com.twu.quiz.mallbackend.service.GoodsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mall/goods")
@CrossOrigin(origins = "*")
public class GoodsController {
    private GoodsService goodsService;

    public GoodsController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Goods goods) {
        goodsService.create(goods);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public ResponseEntity getAll() {
        List<Goods> allGoods = goodsService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(allGoods);
    }
}
