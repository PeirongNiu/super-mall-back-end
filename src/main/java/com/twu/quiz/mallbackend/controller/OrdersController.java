package com.twu.quiz.mallbackend.controller;

import com.twu.quiz.mallbackend.dto.OrdersResponse;
import com.twu.quiz.mallbackend.service.OrdersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mall/orders")
@CrossOrigin("*")
public class OrdersController {
    private OrdersService ordersService;

    public OrdersController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @PostMapping("/{goodsId}")
    public ResponseEntity create(@PathVariable Long goodsId) {
        ordersService.create(goodsId);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public ResponseEntity getAll(){
        List<OrdersResponse> ordersResponseList = ordersService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(ordersResponseList);
    }

    @DeleteMapping("{orderId}")
    public ResponseEntity delete(@PathVariable Long orderId){
        ordersService.delete(orderId);
        return ResponseEntity.status(200).build();
    }
}
