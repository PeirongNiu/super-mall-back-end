package com.twu.quiz.mallbackend.exception;

public class OrderNotExistException extends RuntimeException{
    private String message;
    public OrderNotExistException(String message) {
        super(message);
        this.message = message;
    }
}
