package com.twu.quiz.mallbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.function.Supplier;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class GoodsExistException extends RuntimeException{
    private String message;

    public GoodsExistException(String message) {
        super(message);
        this.message = message;
    }
}
