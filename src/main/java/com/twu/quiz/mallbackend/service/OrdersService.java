package com.twu.quiz.mallbackend.service;

import com.twu.quiz.mallbackend.domain.Goods;
import com.twu.quiz.mallbackend.domain.Orders;
import com.twu.quiz.mallbackend.dto.OrdersResponse;
import com.twu.quiz.mallbackend.exception.GoodsNotExistException;
import com.twu.quiz.mallbackend.exception.OrderNotExistException;
import com.twu.quiz.mallbackend.repository.GoodsRepository;
import com.twu.quiz.mallbackend.repository.OrdersRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrdersService {
    private OrdersRepository ordersRepository;
    private GoodsRepository goodsRepository;

    public OrdersService(OrdersRepository ordersRepository, GoodsRepository goodsRepository) {
        this.ordersRepository = ordersRepository;
        this.goodsRepository = goodsRepository;
    }

    public void create(Long goodsId) {
        Goods goods = goodsRepository.findById(goodsId).orElseThrow(()->new GoodsNotExistException("goods not exist"));
        Optional<Orders> orders = ordersRepository.findByGoods(goods);
        if (!orders.isPresent()){
            ordersRepository.save(new Orders(goods, 1));
            return;
        }
        Orders order = orders.get();
        order.setNum(order.getNum()+1);
        ordersRepository.save(order);
    }

    public List<OrdersResponse> getAll() {
        List<OrdersResponse> list = new ArrayList<>();
        ordersRepository.findAll().stream().forEach(order ->{
            Goods goods = goodsRepository.findById(order.getGoods().getGoodsId()).get();
            OrdersResponse ordersResponse = new OrdersResponse(order.getOrderId(), goods.getName(), goods.getPrice(), goods.getUnit(), order.getNum());
            list.add(ordersResponse);
        });
        return list;
    }

    public void delete(Long orderId) {
        Optional<Orders> orders = ordersRepository.findById(orderId);
        if(!orders.isPresent()){
            throw new OrderNotExistException("order not exist!");
        }
        ordersRepository.deleteById(orderId);
    }
}
