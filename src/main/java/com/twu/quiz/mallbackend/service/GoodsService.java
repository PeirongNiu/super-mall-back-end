package com.twu.quiz.mallbackend.service;

import com.twu.quiz.mallbackend.domain.Goods;
import com.twu.quiz.mallbackend.exception.GoodsExistException;
import com.twu.quiz.mallbackend.repository.GoodsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsService {
    private GoodsRepository goodsRepository;

    public GoodsService(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    public void create(Goods goods) {
        List<Goods> goodsByName = goodsRepository.findByName(goods.getName());
        if (goodsByName.size()!=0)
            throw new GoodsExistException("商品名称已存在,请输入新的商品名称");
        goodsRepository.save(goods);
    }

    public List<Goods> getAll() {
        List<Goods> allGoods = goodsRepository.findAll();
        return allGoods;
    }
}
