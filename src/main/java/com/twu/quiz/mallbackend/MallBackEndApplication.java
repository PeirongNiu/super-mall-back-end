package com.twu.quiz.mallbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MallBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(MallBackEndApplication.class, args);
	}

}
