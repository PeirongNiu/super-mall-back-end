package com.twu.quiz.mallbackend.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

//    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @OneToOne
    @JoinColumn(name = "goods_id")
    private Goods goods;

    @NotNull
    private int num;

    public Orders() {
    }

    public Orders(Goods goods, int num) {
        this.goods = goods;
        this.num = num;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Goods getGoods() {
        return goods;
    }

    public int getNum() {
        return num;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
