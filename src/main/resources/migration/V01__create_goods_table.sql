CREATE TABLE IF NOT EXISTS goods(
    goods_id BIGINT AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    price BIGINT NOT NULL,
    unit VARCHAR(30) NOT NULL,
    img VARCHAR(300) NOT NULL,
    PRIMARY KEY (goods_id)
)engine=innodb auto_increment=1 default charset=utf8 collate=utf8_general_ci;