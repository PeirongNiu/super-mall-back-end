CREATE TABLE IF NOT EXISTS orders(
    order_id BIGINT AUTO_INCREMENT,
    goods_id BIGINT NOT NULL,
    num int NOT NULL,
    PRIMARY KEY (order_id)

)engine=innodb auto_increment=1 default charset=utf8 collate=utf8_general_ci;

ALTER TABLE orders ADD FOREIGN KEY (goods_id) REFERENCES goods(goods_id)