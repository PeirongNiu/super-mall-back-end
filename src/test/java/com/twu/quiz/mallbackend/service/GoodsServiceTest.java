package com.twu.quiz.mallbackend.service;

import com.twu.quiz.mallbackend.domain.Goods;
import com.twu.quiz.mallbackend.exception.GoodsExistException;
import com.twu.quiz.mallbackend.repository.GoodsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
@ActiveProfiles("test")
public class GoodsServiceTest {
    @Autowired
    private GoodsRepository goodsRepository;
    private GoodsService goodsService;

    @BeforeEach
    void setUp() {
        goodsService = new GoodsService(goodsRepository);
    }

    @Test
    void should_save_goods_when_not_exist_same_goods_name() {
        Goods goods = new Goods("可乐", 3, "听", "aaa");

        goodsService.create(goods);

        Goods goodsById = goodsRepository.findById(goods.getGoodsId()).get();
        assertEquals(goods, goodsById);
    }

    @Test
    void should_throws_exception_when_exist_same_goods_name() {
        Goods goods = new Goods("可乐", 3, "听", "aaa");
        goodsRepository.save(goods);

        assertThrows(GoodsExistException.class, ()->{
            goodsService.create(goods);
        });
    }

    @Test
    void should_get_all_goods() {
        Goods goods = new Goods("可乐", 3, "听", "aaa");
        goodsRepository.save(goods);

        goodsService = new GoodsService(goodsRepository);
        List<Goods> allGoods = goodsService.getAll();

        assertEquals(goods, allGoods.get(0));
    }
}
