package com.twu.quiz.mallbackend.service;


import com.twu.quiz.mallbackend.domain.Goods;
import com.twu.quiz.mallbackend.domain.Orders;
import com.twu.quiz.mallbackend.dto.OrdersResponse;
import com.twu.quiz.mallbackend.exception.GoodsNotExistException;
import com.twu.quiz.mallbackend.exception.OrderNotExistException;
import com.twu.quiz.mallbackend.repository.GoodsRepository;
import com.twu.quiz.mallbackend.repository.OrdersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
@ActiveProfiles("test")
public class OrdersServiceTest {

    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    private OrdersService ordersService;

    @BeforeEach
    void setUp() {
        ordersService = new OrdersService(ordersRepository, goodsRepository);
    }

    @Test
    void should_create_order_success() {
        Goods goods = new Goods("可乐", 3L, "听", "aaa");
        goodsRepository.save(goods);
        Long goodsId = goodsRepository.findByName("可乐").get(0).getGoodsId();
        ordersService.create(goodsId);

        assertNotNull(ordersRepository.findByGoods(goods));
    }

    @Test
    void should_create_order_failed_when_goods_not_exist() {
        assertThrows(GoodsNotExistException.class, () -> {
            ordersService.create(1L);
        });
    }

    @Test
    void should_get_all_orders_when_have_orders() {
        Goods goods = new Goods("可乐", 3L, "听", "aaa");
        goodsRepository.save(goods);
        Orders orders = new Orders(goods, 2);
        ordersRepository.save(orders);

        List<OrdersResponse> list = ordersService.getAll();

        assertEquals("可乐", list.get(0).getName());
        assertEquals(3L, list.get(0).getPrice());
        assertEquals("听", list.get(0).getUnit());
        assertEquals(2, list.get(0).getNum());

    }

    @Test
    void should_delete_order_by_id_when_order_exist() {
        Goods goods = new Goods("可乐", 3L, "听", "aaa");
        goodsRepository.save(goods);
        Orders orders = new Orders(goods, 2);
        Orders saveOrder = ordersRepository.save(orders);
        Long orderId = saveOrder.getOrderId();

        ordersService.delete(orderId);

        assertFalse(ordersRepository.findByGoods(goods).isPresent());
    }

    @Test
    void should_delete_order_by_id_when_order_not_exist() {
        assertThrows(OrderNotExistException.class, ()->{
            ordersService.delete(1L);
        });
    }
}
