package com.twu.quiz.mallbackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.twu.quiz.mallbackend.domain.Goods;
import com.twu.quiz.mallbackend.repository.GoodsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class GoodsControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private GoodsRepository goodsRepository;

    @Test
    public void should_create_goods_return_201() throws Exception {
        Goods goods = new Goods("雪碧", 3, "听", "aaaa");
        ObjectMapper objectMapper = new ObjectMapper();
        String goodsString = objectMapper.writeValueAsString(goods);
        mockMvc.perform(post("/mall/goods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(goodsString))
                .andExpect(status().isCreated());
    }

    @Test
    void should_get_goods_return_200() throws Exception {
        Goods goods = new Goods("可乐", 3, "听", "aaaa");
        goodsRepository.save(goods);
        mockMvc.perform(get("/mall/goods")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value(goods.getName()))
                .andExpect(jsonPath("$[0].price").value(goods.getPrice()))
                .andExpect(jsonPath("$[0].unit").value(goods.getUnit()))
                .andExpect(jsonPath("$[0].img").value(goods.getImg()));
    }
}
