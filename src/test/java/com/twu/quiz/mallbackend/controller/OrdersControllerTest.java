package com.twu.quiz.mallbackend.controller;

import com.twu.quiz.mallbackend.dto.OrdersResponse;
import com.twu.quiz.mallbackend.service.OrdersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class OrdersControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private OrdersController ordersController;

    @MockBean
    private OrdersService ordersService;



    @Test
    void should_return_201_when_create_orders() throws Exception {
        doNothing().when(ordersService).create(any());
        mockMvc.perform(post("/mall/orders/001")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());
    }

    @Test
    void should_call_service_method_when_run_create_method() {
        doNothing().when(ordersService).create(any());
        ordersController.create(1L);
        verify(ordersService, times(1)).create(anyLong());

    }

    @Test
    void should_get_all_orders_return_200() throws Exception {
        OrdersResponse ordersResponse = new OrdersResponse(1L, "可乐", 3L, "听", 2);
        List<OrdersResponse> list = new ArrayList<>();
        list.add(ordersResponse);
        when(ordersService.getAll()).thenReturn(list);

        mockMvc.perform(get("/mall/orders")
        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("可乐"))
                .andExpect(jsonPath("$[0].unit").value("听"))
                .andExpect(jsonPath("$[0].num").value(2))
                .andExpect(jsonPath("$[0].price").value(3L));
    }

    @Test
    void should_call_delete_method() {
        doNothing().when(ordersService).delete(anyLong());

        ordersController.delete(anyLong());

        verify(ordersService, times(1)).delete(anyLong());
    }

    @Test
    void should_delete_order_by_order_id() throws Exception {
        doNothing().when(ordersService).delete(anyLong());

        mockMvc.perform(delete("/mall/orders/1"))
                .andExpect(status().isOk());
    }
}
